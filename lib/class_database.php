<?php
namespace Goltana;

class DataBase
{
	private $pdo;
	private static $instance = null;

	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance->pdo;
	}

	private function __construct()
	{
		$this->pdo = new \PDO(
			'mysql:host=' . Config::$dbHost . ';dbname=' . Config::$dbName . ';charset=' . Config::$dbCharset,
			Config::$dbUser,
			Config::$dbPass,
			[
				\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
			]
		);
	}

	// метод должен быть пустым, чтобы предотвратить дублирование соединения
	private function __clone()
	{
	}

	public function __destruct()
	{
		if ($this->pdo !== null) {
			$this->pdo = null;
		}
	}
}
