<?php
namespace Goltana;

class Relation extends Common
{
	public function sendValue($heroId, $heroIdSubject, $relationKey, $relationValue)
	{
		if (($relationKey !== 'ally') && ($relationKey !== 'enemy')) {
			return false;
		}
		if (($heroId == $heroIdSubject) || (!is_string($relationValue))) {
			return false;
		}
		if ($relation = $this->getRelationIfExists($heroId, $heroIdSubject)) {
			if ($relation["subject_is_$relationKey"] === $relationValue) {
				$otherRelationKey = ($relationKey === 'enemy')? 'ally' : 'enemy';
				if (!$relation["subject_is_$otherRelationKey"]) {
					$this->deleteRelation($heroId, $heroIdSubject);
					return 'delete';
				} else {
					$this->updateRelation($heroId, $heroIdSubject, $relationKey, null);
					return 'delete';
				}
			} else {
				$this->updateRelation($heroId, $heroIdSubject, $relationKey, $relationValue);
				if ($relation["subject_is_$relationKey"]) {
					return 'toggle';
				} else {
					return 'add';
				}
			}
		} else {
			$this->insertRelation($heroId, $heroIdSubject, $relationKey, $relationValue);
			return 'add';
		}
	}

	public function getRelations()
	{
		$stmt = $this->pdo->prepare(
			"SELECT hero_id, hero_id_subject, subject_is_ally, subject_is_enemy FROM $this->tableName " .
			"WHERE steam_id = ?"
		);
		$stmt->bindParam(1, $_SESSION['steam']['id']);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	private function deleteRelation($heroId, $heroIdSubject)
	{
		$stmt = $this->pdo->prepare(
			"DELETE FROM $this->tableName WHERE steam_id = ? AND hero_id = ? AND hero_id_subject = ?"
		);
		$data = [
			$_SESSION['steam']['id'],
			$heroId,
			$heroIdSubject
		];
		return $stmt->execute($data);
	}

	private function updateRelation($heroId, $heroIdSubject, $relationKey, $relationValue)
	{
		$stmt = $this->pdo->prepare(
			"UPDATE $this->tableName SET subject_is_$relationKey = ? " .
			"WHERE steam_id = ? AND hero_id = ? AND hero_id_subject = ?"
		);
		$data = [
			$relationValue,
			$_SESSION['steam']['id'],
			$heroId,
			$heroIdSubject
		];
		return $stmt->execute($data);
	}

	private function insertRelation($heroId, $heroIdSubject, $relationKey, $relationValue)
	{
		$stmt = $this->pdo->prepare("INSERT INTO $this->tableName VALUES (?, ?, ?, ?, ?)");
		$data = [
			$_SESSION['steam']['id'],
			$heroId,
			$heroIdSubject,
			($relationKey === 'ally')? $relationValue : null,
			($relationKey === 'enemy')? $relationValue : null,
		];
		return $stmt->execute($data);
	}

	private function getRelationIfExists($heroId, $heroIdSubject)
	{
		$stmt = $this->pdo->prepare(
			"SELECT * FROM $this->tableName WHERE steam_id = ? AND hero_id = ? AND hero_id_subject = ?"
		);
		$data = [
			$_SESSION['steam']['id'],
			$heroId,
			$heroIdSubject
		];
		$stmt->execute($data);
		if ($row = $stmt->fetch()) {
			return $row;
		} else {
			return false;
		}
	}
}
