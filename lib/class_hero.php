<?php
namespace Goltana;

class Hero extends Common
{
	public function getHeroesList()
	{
		$stmt = $this->pdo->prepare('SELECT * FROM ' . $this->tableName . ' ORDER BY hero_name');
		$stmt->execute();
		return $stmt->fetchAll();
	}
}
