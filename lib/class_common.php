<?php
namespace Goltana;

class Common
{
	protected $pdo;
	protected $tableName;

	public function __construct()
	{
		$this->pdo = DataBase::getInstance();

		$arr = explode('\\', get_class($this));
		$this->tableName = Config::$dbTablePrefix . mb_strtolower($arr[count($arr) - 1]);
	}
}
