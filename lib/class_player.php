<?php
namespace Goltana;

class Player extends Common
{
	public function existsPlayerId($id)
	{
		$stmt = $this->pdo->prepare('SELECT steam_id FROM ' . $this->tableName . ' WHERE steam_id = ?');
		// TODO заменить на bindValue
		$stmt->bindParam(1, $id);
		$stmt->execute();
		if ($stmt->fetchAll(\PDO::FETCH_COLUMN)) {
			return true;
		} else {
			return false;
		}
	}

	/** @var array $player */
	public function createPlayerIfNotExists($player)
	{
		if (!$this->existsPlayerId($player['id'])) {
			$stmt = $this->pdo->prepare('INSERT INTO ' . $this->tableName . ' VALUES (?, ?, ?, ?)');
			$data = [
				$player['id'],
				$player['nickname'],
				$player['profile'],
				$player['avatar']
			];
			$stmt->execute($data);
		}
	}
}
