<?php
function myAutoload($className)
{
	$className = str_replace('Goltana\\', '', $className);
	require __DIR__ . '/class_' . mb_strtolower($className) . '.php';
}
spl_autoload_register('myAutoload');
