export default class Search
{
	constructor(dom, heroList)
	{
		this.globalDom = dom;
		this.heroList = this.addDomNumbersInHeroList(heroList);
		this.currentSearch = [];
	}

	addDomNumbersInHeroList(heroList)
	{
		heroList.forEach((item, i) => {
			heroList[i].domNumber = i;
		});
		return heroList;
	}

	search(heroName) {
		let globalDom = this.globalDom;
		if (this.currentSearch.length !== 0) {
			this.currentSearch.forEach((item) => {
				globalDom.heroList.children[item.domNumber].classList.remove('found');
			});
		}
		if (heroName === '') {
			this.currentSearch = [];
			globalDom.heroList.classList.remove('hero-list_mode_search');
			return;
		} else {
			globalDom.heroList.classList.add('hero-list_mode_search');
		}
		this.currentSearch = this.heroList.filter((hero) => {
			return hero.hero_name.toLowerCase().indexOf(heroName.toLowerCase()) !== -1;
		});
		this.currentSearch.forEach((item) => {
			globalDom.heroList.children[item.domNumber].classList.add('found');
		});
	}

	clearSearch()
	{
		let globalDom = this.globalDom;
		if (this.currentSearch.length !== 0) {
			this.currentSearch.forEach((item) => {
				globalDom.heroList.children[item.domNumber].classList.remove('found');
			});
		}
		globalDom.searchHero.value = '';
		this.currentSearch = [];
		globalDom.heroList.classList.remove('hero-list_mode_search');
	}
}