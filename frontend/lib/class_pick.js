export default class Pick
{
	constructor(dom)
	{
		this.globalDom = dom;
		this.left = {
			pick: [{}, {}, {}, {}, {}],
			isYourTeam: true
		};
		this.right = {
			pick: [{}, {}, {}, {}, {}],
			isYourTeam: false
		};
		this.ban = [];
	}

	existsHero(teamSide, heroId)
	{
		for (let i = 0; i < this[teamSide].pick.length; i++ ) {
			if (JSON.stringify(this[teamSide].pick[i]) !== '{}') {
				if (this[teamSide].pick[i].id === heroId) {
					return {
						teamSide: teamSide,
						heroTeamDomNumber: i,
						heroListDomNumber: this.globalDom.team[teamSide].children[i].dataset.heroListDomNumber
					};
				}
			}
		}
		return false;
	}

	existsFreePlace(teamSide)
	{
		for (let i = 0; i < this[teamSide].pick.length; i++ ) {
			if (JSON.stringify(this[teamSide].pick[i]) === '{}') {
				return {
					teamSide: teamSide,
					heroTeamDomNumber: i
				};
			}
		}
		return false;
	}

	removeHero(hero, relationInvert)
	{
		const { teamSide, heroTeamDomNumber, heroListDomNumber } = hero;

		let heroId = this[teamSide].pick[heroTeamDomNumber].id;
		this[teamSide].pick[heroTeamDomNumber] = {};

		let teamDom = this.globalDom.team[teamSide];
		teamDom.children[heroTeamDomNumber].style.backgroundImage = `url("/img/hero/dota.jpg")`;
		teamDom.children[heroTeamDomNumber].title = '';
		teamDom.children[heroTeamDomNumber].children[0].classList.add('hidden');
		teamDom.children[heroTeamDomNumber].children[1].classList.add('hidden');
		teamDom.children[heroTeamDomNumber].dataset.heroListDomNumber = '';

		let heroList = this.globalDom.heroList;
		let heroDom = heroList.children[heroListDomNumber];
		heroDom.classList.remove('picked');
		let heroButton = heroDom.querySelector(`.js-add-to-${ teamSide }-team`);
		heroButton.classList.remove('checked');

		this.calculateAndSetScopeByHero(heroId, teamSide, relationInvert, 'cancel');
	}

	addHero(hero, relationInvert)
	{
		const { teamSide, heroTeamDomNumber, heroId, heroName, heroListDomNumber } = hero;

		this[teamSide].pick[heroTeamDomNumber] = {
			id: heroId,
			name: heroName
		};

		let teamDom = this.globalDom.team[teamSide];
		teamDom.children[heroTeamDomNumber].style.backgroundImage = `url("/img/hero/${ heroName.toLowerCase().replace(/ /g, '_') }.jpg")`;
		teamDom.children[heroTeamDomNumber].title = heroName;
		teamDom.children[heroTeamDomNumber].children[0].classList.remove('hidden');
		teamDom.children[heroTeamDomNumber].children[1].classList.remove('hidden');
		teamDom.children[heroTeamDomNumber].dataset.heroListDomNumber = heroListDomNumber;

		let heroList = this.globalDom.heroList;
		let heroDom = heroList.children[heroListDomNumber];
		heroDom.classList.add('picked');
		let heroButton = heroDom.querySelector(`.js-add-to-${ teamSide }-team`);
		heroButton.classList.add('checked');

		this.calculateAndSetScopeByHero(heroId, teamSide, relationInvert, 'apply');
	}

	calculateAndSetScopeByHero(heroId, teamSide, relationInvert, command)
	{
		if (relationInvert[heroId] !== undefined) {
			let heroList = this.globalDom.heroList;
			let heroObject = relationInvert[heroId];
			let teamKey = (this[teamSide].isYourTeam) ? 'ally' : 'enemy';
			for (let key in heroObject) {
				if (heroObject[key][teamKey] !== undefined) {
					let heroListDomNumber = this.globalDom.heroIdDomNumbers[key];
					let heroDom = heroList.children[heroListDomNumber];
					let scoreDom = heroDom.querySelector('.js-score');
					let score = Number(scoreDom.innerHTML);
					if (command === 'apply') {
						if (heroObject[key][teamKey] === 'good') {
							score++;
						} else if (heroObject[key][teamKey] === 'bad') {
							score--;
						}
					} else if (command === 'cancel') {
						if (heroObject[key][teamKey] === 'good') {
							score--;
						} else if (heroObject[key][teamKey] === 'bad') {
							score++;
						}
					}
					scoreDom.innerHTML = score;
					if (score > 0) {
						if (scoreDom.classList.contains('red')) {
							scoreDom.classList.remove('red');
						}
						if (!scoreDom.classList.contains('green')) {
							scoreDom.classList.add('green');
						}
						if (!heroDom.classList.contains('recommendation')) {
							heroDom.classList.add('recommendation');
						}
					} else if (score < 0) {
						if (scoreDom.classList.contains('green')) {
							scoreDom.classList.remove('green');
						}
						if (!scoreDom.classList.contains('red')) {
							scoreDom.classList.add('red');
						}
						if (!heroDom.classList.contains('recommendation')) {
							heroDom.classList.add('recommendation');
						}
					} else {
						if (scoreDom.classList.contains('green')) {
							scoreDom.classList.remove('green');
						} else if (scoreDom.classList.contains('red')) {
							scoreDom.classList.remove('red');
						}
						if (heroDom.classList.contains('recommendation')) {
							heroDom.classList.remove('recommendation');
						}
					}
				}
			}
		}
	}

	reCalculateAllScopes(relationInvert, command)
	{
		let teamSides = ['left', 'right'];
		for (let i = 0; i < teamSides.length; i++) {
			for (let key in this[teamSides[i]].pick) {
				if (JSON.stringify(this[teamSides[i]].pick[key] !== '{}')) {
					this.calculateAndSetScopeByHero(
						this[teamSides[i]].pick[key].id,
						[teamSides[i]],
						relationInvert,
						command
					);
				}
			}
		}
	}

	teamSwitch(relationInvert)
	{
		this.reCalculateAllScopes(relationInvert, 'cancel');
		this.left.isYourTeam = !this.left.isYourTeam;
		this.right.isYourTeam = !this.right.isYourTeam;
		this.reCalculateAllScopes(relationInvert, 'apply');

		let teamSwitchToggles = this.globalDom.teamSwitch.querySelectorAll('.team-switch__toggle');
		for (let i = 0; i < teamSwitchToggles.length; i++) {
			if (teamSwitchToggles[i].classList.contains('checked')) {
				teamSwitchToggles[i].classList.remove('checked');
				teamSwitchToggles[i].title = 'вражеская команда';
			} else {
				teamSwitchToggles[i].classList.add('checked');
				teamSwitchToggles[i].title = 'моя команда';
			}
		}
	}

	clearPick(relationInvert)
	{
		let teams = ['left', 'right'];
		for (let j = 0; j < teams.length; j++) {
			for (let i = 0; i < this[teams[j]].pick.length; i++ ) {
				if (JSON.stringify(this[teams[j]].pick[i]) !== '{}') {
					this.removeHero(
						{
							teamSide: teams[j],
							heroTeamDomNumber: i,
							heroListDomNumber: this.globalDom.team[teams[j]].children[i].dataset.heroListDomNumber
						},
						relationInvert
					);
				}
			}
		}
	}

	switchHeroBan(hero)
	{
		const { heroId, heroListDomNumber } = hero;
		let banButton = this.globalDom.heroList.children[heroListDomNumber].querySelector('.js-add-to-ban');
		let i = this.ban.indexOf(heroId);
		if (i === -1) {
			this.ban.push(heroId);
			banButton.classList.add('checked');
			banButton.parentElement.classList.add('banned');
		} else {
			this.ban.splice(i, 1);
			banButton.classList.remove('checked');
			banButton.parentElement.classList.remove('banned');
		}
	}

	removeHeroFromBanIfExists(heroId, heroListDomNumber)
	{
		let i = this.ban.indexOf(heroId);
		if (i !== -1) {
			this.ban.splice(i, 1);
			let banButton = this.globalDom.heroList.children[heroListDomNumber].querySelector('.js-add-to-ban');
			banButton.classList.remove('checked');
		}
	}
}
