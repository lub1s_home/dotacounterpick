export default class Tune
{
	constructor(dom, relation)
	{
		this.globalDom = dom;
		this.heroId = null;
		this.relation = relation;
		this.relationInvert = this.invertKeysInRelation();
	}

	invertKeysInRelation()
	{
		let relation = this.relation;
		let relationInvert = {};
		for (let key in relation) {
			for (let deepKey in relation[key]) {
				if (relationInvert[deepKey] === undefined) {
					relationInvert[deepKey] = {};
				}
				if (relationInvert[deepKey][key] === undefined) {
					relationInvert[deepKey][key] = {};
				}
				relationInvert[deepKey][key] = relation[key][deepKey];
			}
		}
		return relationInvert;
	}

	tuneHero(heroId, heroName)
	{
		this.heroId = heroId;

		this.globalDom.team.left.classList.add('hidden');
		this.globalDom.team.right.classList.add('hidden');
		this.globalDom.heroSettings.classList.remove('hidden');
		this.globalDom.heroSettingsHero.style.backgroundImage = `url("/img/hero/${ heroName.toLowerCase().replace(/ /g, '_') }.jpg")`;
		this.globalDom.heroSettingsName.innerHTML = heroName;
		let heroList = this.globalDom.heroList;
		heroList.classList.remove('hero-list_mode_pick');
		heroList.classList.add('hero-list_mode_config');
		heroList.children[this.globalDom.heroIdDomNumbers[heroId]].classList.add('hero-self');

		let heroObject = this.relation[heroId];
		this.switchTogglesOnHero(heroList, heroObject, true);
	}

	returnToPick()
	{
		this.globalDom.team.left.classList.remove('hidden');
		this.globalDom.team.right.classList.remove('hidden');
		this.globalDom.heroSettings.classList.add('hidden');
		this.globalDom.heroList.classList.remove('hero-list_mode_config');
		this.globalDom.heroList.classList.add('hero-list_mode_pick');

		let heroList = this.globalDom.heroList;
		let heroObject = this.relation[this.heroId];
		heroList.children[this.globalDom.heroIdDomNumbers[this.heroId]].classList.remove('hero-self');
		this.switchTogglesOnHero(heroList, heroObject, false);
	}

	switchTogglesOnHero(heroList, heroObject, on)
	{
		for (let key in heroObject) {
			let heroListDomNumber = this.globalDom.heroIdDomNumbers[key];
			let hero = heroList.children[heroListDomNumber];
			let heroSubject = heroObject[key];
			for (let deepKey in heroSubject) {
				let toggle = hero.querySelector(`.js-relation-${ deepKey }-${ heroSubject[deepKey] }`);
				if (on) {
					toggle.classList.add('checked');
					if (!hero.classList.contains('tuned')) {
						hero.classList.add('tuned');
					}
				} else {
					toggle.classList.remove('checked');
					if (hero.classList.contains('tuned')) {
						hero.classList.remove('tuned');
					}
				}
			}
		}
	}

	setRelation(heroIdSubject, key, value, pick)
	{
		let xhr = new XMLHttpRequest();
		xhr.open('POST', 'ajax.php', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.send(
			'action=relationSendValue' +
			'&heroId=' + encodeURIComponent(this.heroId) +
			'&heroIdSubject=' + encodeURIComponent(heroIdSubject) +
			'&key=' + encodeURIComponent(key) +
			'&value=' + encodeURIComponent(value)
		);
		xhr.onreadystatechange = () => {
			if (xhr.readyState === 4) {
				let heroListDomNumber = this.globalDom.heroIdDomNumbers[heroIdSubject];
				let hero = this.globalDom.heroList.children[heroListDomNumber];
				let toggle = hero.querySelector(`.js-relation-${ key }-${ value }`);
				switch (xhr.responseText) {
					case 'add':
						if (this.relation[this.heroId] === undefined) {
							this.relation[this.heroId] = {};
						}
						if (this.relation[this.heroId][heroIdSubject] === undefined) {
							this.relation[this.heroId][heroIdSubject] = {};
							hero.classList.add('tuned');
						}
						this.relation[this.heroId][heroIdSubject][key] = value;
						if (this.relationInvert[heroIdSubject] === undefined) {
							this.relationInvert[heroIdSubject] = {};
						}
						if (this.relationInvert[heroIdSubject][this.heroId] === undefined) {
							this.relationInvert[heroIdSubject][this.heroId] = {};
						}
						this.relationInvert[heroIdSubject][this.heroId][key] = value;
						toggle.classList.add('checked');
						this.reCalculateHeroIfHeExistsInPick(key, pick, heroIdSubject, 'apply');
						break;
					case 'delete':
						let otherKey = (key === 'ally')? 'enemy' : 'ally';
						if (this.relation[this.heroId][heroIdSubject][otherKey] !== undefined) {
							delete this.relation[this.heroId][heroIdSubject][key];
						} else {
							delete this.relation[this.heroId][heroIdSubject];
							if (JSON.stringify(this.relation[this.heroId]) === '{}') {
								delete this.relation[this.heroId];
							}
							hero.classList.remove('tuned');
						}
						this.reCalculateHeroIfHeExistsInPick(key, pick, heroIdSubject, 'cancel');
						if (this.relationInvert[heroIdSubject][this.heroId][otherKey] !== undefined) {
							delete this.relationInvert[heroIdSubject][this.heroId][key];
						} else {
							delete this.relationInvert[heroIdSubject][this.heroId];
							if (JSON.stringify(this.relationInvert[heroIdSubject]) === '{}') {
								delete this.relationInvert[heroIdSubject];
							}
						}
						toggle.classList.remove('checked');
						break;
					case 'toggle':
						this.relation[this.heroId][heroIdSubject][key] = value;
						this.reCalculateHeroIfHeExistsInPick(key, pick, heroIdSubject, 'cancel');
						this.relationInvert[heroIdSubject][this.heroId][key] = value;
						this.reCalculateHeroIfHeExistsInPick(key, pick, heroIdSubject, 'apply');
						let otherValue = (value === 'good')? 'bad' : 'good';
						let toggleOtherValue = hero.querySelector(`.js-relation-${ key }-${ otherValue }`);
						toggle.classList.add('checked');
						toggleOtherValue.classList.remove('checked');
						break;
				}
			}
		};
	}

	reCalculateHeroIfHeExistsInPick(key, pick, heroIdSubject, command)
	{
		let teamSideForCheckHero;
		if (key === 'ally') {
			teamSideForCheckHero = (pick.left.isYourTeam === true) ? 'left' : 'right';
		} else if (key === 'enemy') {
			teamSideForCheckHero = (pick.left.isYourTeam === true) ? 'right' : 'left';
		} else {
			return
		}
		if (pick.existsHero(teamSideForCheckHero, heroIdSubject)) {
			pick.calculateAndSetScopeByHero(
				heroIdSubject,
				teamSideForCheckHero,
				this.relationInvert,
				command
			);
		}
	}

	showRelationsBetweenHeroAndPick(heroId, heroName, pick)
	{
		this.hideRelationsBetweenHeroAndPick();
		if (this.relation[heroId] === undefined) {
			return;
		}
		let info = {
			'ally': [],
			'enemy': []
		};
		let heroRelations = this.relation[heroId];
		let teamSides = [
			(pick.left.isYourTeam === true) ? 'left'  : 'right',
			(pick.left.isYourTeam === true) ? 'right' : 'left'
		];
		for (let j = 0; j < teamSides.length; j++) {
			let teamSidesPick = pick[teamSides[j]].pick;
			let teamKey = (pick[teamSides[j]].isYourTeam === true) ? 'ally' : 'enemy';
			for (let i in teamSidesPick) {
				if ((teamSidesPick[i].id !== undefined) &&
					(heroRelations[teamSidesPick[i].id] !== undefined) &&
					(heroRelations[teamSidesPick[i].id][teamKey] !== undefined)
				) {
					info[teamKey].push({
						'name': teamSidesPick[i].name,
						'assessment': heroRelations[teamSidesPick[i].id][teamKey]
					});
				}
			}
			let arInfoScore = this.globalDom.arInfoScore;
			let heroSpan   = arInfoScore[j].querySelector('.js-info-score__hero');
			let heroesSpan = arInfoScore[j].querySelector('.js-info-score__heroes');
			if (info[teamKey].length > 0) {
				for (let i = 0; i < info[teamKey].length; i++ ) {
					let comma = document.createTextNode(',');
					let span = document.createElement('span');
					if (i === 0) {
						heroSpan.innerHTML = heroName;
					} else {
						heroesSpan.appendChild(comma);
					}
					span.classList.add('info-score__item');
					span.classList.add(`info-score__item_${ info[teamKey][i].assessment }`);
					span.innerHTML = info[teamKey][i].name;
					heroesSpan.appendChild(span);
				}
				arInfoScore[j].classList.remove('hidden');
			}
		}
	}

	hideRelationsBetweenHeroAndPick()
	{
		let arInfoScore = this.globalDom.arInfoScore;
		for (let i = 0; i < arInfoScore.length; i++) {
			arInfoScore[i].classList.add('hidden');
			arInfoScore[i].querySelector('.js-info-score__hero').innerHTML = '';
			arInfoScore[i].querySelector('.js-info-score__heroes').innerHTML = '';
		}
	}
}
