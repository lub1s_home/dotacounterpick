export default class Dom
{
	constructor()
	{
		this.heroList           = document.querySelector('#js-hero-list');
		this.heroSettings       = document.querySelector('#js-hero-settings');
		this.heroSettingsHero   = this.heroSettings.querySelector('.hero');
		this.heroSettingsName   = this.heroSettings.querySelector('.hero-settings__name');
		this.heroIdDomNumbers   = this.getHeroIdDomNumbers();
		this.team               = {};
		this.team.left          = document.querySelector('#js-left-team');
		this.team.right         = document.querySelector('#js-right-team');
		this.teamSwitch         = document.querySelector('#js-team-switch');
		this.returnToPickButton = document.querySelector('#js-return-to-pick');
		this.searchHero         = document.querySelector('#js-search-hero');
		this.arInfoScore        = document.querySelectorAll('.js-info-score');
	}

	getHeroIdDomNumbers()
	{
		let heroIdDomNumber = {};
		let length = this.heroList.children.length;
		for (let i = 0; i < length; i++) {
			heroIdDomNumber[this.heroList.children[i].dataset.heroId] = i;
		}
		return heroIdDomNumber;
	}
}
