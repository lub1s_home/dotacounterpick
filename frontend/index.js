'use strict';

import './main.less';
import Dom    from './lib/class_dom';
import Pick   from './lib/class_pick';
import Tune   from './lib/class_tune';
import Search from './lib/class_search';

let relationList = JSON.parse(jsonRelationList);
let relation = {};
relationList.forEach((item) => {
	if (relation[item.hero_id] === undefined) {
		relation[item.hero_id] = {};
	}
	if (relation[item.hero_id][item.hero_id_subject] === undefined) {
		relation[item.hero_id][item.hero_id_subject] = {};
	}
	if (item.subject_is_ally !== null) {
		relation[item.hero_id][item.hero_id_subject].ally = item.subject_is_ally;
	}
	if (item.subject_is_enemy !== null) {
		relation[item.hero_id][item.hero_id_subject].enemy = item.subject_is_enemy;
	}
});

const dom    = new Dom();
const pick   = new Pick(dom);
const tune   = new Tune(dom, relation);
const search = new Search(dom, JSON.parse(jsonHeroList));

dom.heroList.onclick = (event) => {
	let selectedTeamSide;
	let anotherTeamSide;
	let target = event.target;
	if (target.classList.contains('js-score')) {
		tune.showRelationsBetweenHeroAndPick(
			target.parentElement.dataset.heroId,
			target.parentElement.dataset.heroName,
			pick
		);
		return;

	}
	if (target.classList.contains('js-relation')) {
		tune.setRelation(
			target.parentElement.dataset.heroId,
			target.dataset.key,
			target.dataset.value,
			pick
		);
		return;

	}
	if (target.classList.contains('js-tune-hero')) {
		let heroId = target.parentElement.dataset.heroId;
		let heroName = target.parentElement.dataset.heroName;
		tune.tuneHero(heroId, heroName);
		search.clearSearch();
		tune.hideRelationsBetweenHeroAndPick();
		return;

	} else if (target.classList.contains('js-add-to-ban')) {
		let heroId = target.parentElement.dataset.heroId;
		let hero = pick.existsHero('left', heroId);
		if (typeof hero === "object") {
			pick.removeHero(hero, tune.relationInvert);
		}
		hero = pick.existsHero('right', heroId);
		if (typeof hero === "object") {
			pick.removeHero(hero, tune.relationInvert);
		}
		pick.switchHeroBan({
			heroId: heroId,
			heroListDomNumber: target.parentElement.dataset.heroListDomNumber
		});
		return;

	} else if (target.classList.contains('js-add-to-left-team')) {
		selectedTeamSide = 'left';
		anotherTeamSide = 'right';
	} else if (target.classList.contains('js-add-to-right-team')) {
		selectedTeamSide = 'right';
		anotherTeamSide = 'left';
	} else {
		return;

	}
	let heroId = target.parentElement.dataset.heroId;
	let hero = pick.existsHero(selectedTeamSide, heroId);
	if (typeof hero === "object") {
		pick.removeHero(hero, tune.relationInvert);
	} else {
		let teamPlace = pick.existsFreePlace(selectedTeamSide);
		if (typeof teamPlace === "object") {
			hero = pick.existsHero(anotherTeamSide, heroId);
			if (typeof hero === "object") {
				pick.removeHero(hero, tune.relationInvert);
			}
			let heroName = target.parentElement.dataset.heroName;
			let heroListDomNumber = target.parentElement.dataset.heroListDomNumber;
			pick.removeHeroFromBanIfExists(heroId, heroListDomNumber);
			pick.addHero(
				{
					teamSide: teamPlace.teamSide,
					heroTeamDomNumber: teamPlace.heroTeamDomNumber,
					heroId: heroId,
					heroName: heroName,
					heroListDomNumber: heroListDomNumber
				},
				tune.relationInvert
			);
		}
	}
	search.clearSearch();
	tune.hideRelationsBetweenHeroAndPick();
};

dom.team.left.onclick = dom.team.right.onclick = (event) => {
	let target = event.target;
	if (target.classList.contains('js-remove-from-pick')) {
		pick.removeHero(
			{
				teamSide: target.parentElement.parentElement.dataset.teamSide,
				heroTeamDomNumber: target.parentElement.dataset.heroTeamDomNumber,
				heroListDomNumber: target.parentElement.dataset.heroListDomNumber
			},
			tune.relationInvert
		);
		tune.hideRelationsBetweenHeroAndPick();
	} else if (target.classList.contains('js-move-to-another-team')) {
		let teamPlace = pick.existsFreePlace(target.dataset.moveToTeamSide);
		if (typeof teamPlace === "object") {
			let heroListDomNumber = target.parentElement.dataset.heroListDomNumber;
			pick.removeHero(
				{
					teamSide: target.parentElement.parentElement.dataset.teamSide,
					heroTeamDomNumber: target.parentElement.dataset.heroTeamDomNumber,
					heroListDomNumber: heroListDomNumber,
				},
				tune.relationInvert
			);
			pick.addHero(
				{
					teamSide: teamPlace.teamSide,
					heroTeamDomNumber: teamPlace.heroTeamDomNumber,
					heroId: dom.heroList.children[heroListDomNumber].dataset.heroId,
					heroName: dom.heroList.children[heroListDomNumber].dataset.heroName,
					heroListDomNumber: heroListDomNumber
				},
				tune.relationInvert
			);
			tune.hideRelationsBetweenHeroAndPick();
		}
	}
};

dom.teamSwitch.onclick = (event) => {
	let target = event.target;
	if (target.classList.contains('team-switch__toggle') && !target.classList.contains('checked')) {
		pick.teamSwitch(tune.relationInvert);
		tune.hideRelationsBetweenHeroAndPick();
	} else if (target.classList.contains('js-clear-pick')) {
		pick.clearPick(tune.relationInvert);
		tune.hideRelationsBetweenHeroAndPick();
	}
};

dom.returnToPickButton.onclick = () => {
	tune.returnToPick();
	search.clearSearch();
};

dom.searchHero.oninput = (event) => {
	search.search(event.target.value);
};

// для теста
//TODO удалить перед production
document.querySelector('h1').onclick = () => {
};
