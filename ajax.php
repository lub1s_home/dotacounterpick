<?php
session_start();
require_once 'lib/autoload.php';

use Goltana\Relation;

$objRelation = new Relation();
if (($_POST['heroId'] === 'null') || ($_POST['heroIdSubject'] === 'null')) {
	return;
}
if (isset($_POST['action']) && $_POST['action'] === 'relationSendValue') {
	$response = $objRelation->sendValue($_POST['heroId'], $_POST['heroIdSubject'], $_POST['key'], $_POST['value']);
	echo $response;
}
