<?php
session_start();
require_once 'vendor/autoload.php';
require_once 'lib/autoload.php';

use Goltana\Config;
use Goltana\Player;
use Goltana\Hero;
use Goltana\Relation;

try {
	$openId = new LightOpenID('dotacp.lc');
	if (isset($_GET['login'])) {
		$openId->identity = 'http://steamcommunity.com/openid';
		header('Location: ' . $openId->authUrl());
	} elseif (isset($_GET['logout'])) {
		$_SESSION = [];
	}
	if ($openId->mode) {
		if ($openId->validate()) {
			$id = str_replace('http://steamcommunity.com/openid/id/', '', $openId->identity);
			$url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/' .
				'?key=' . Config::$steamApiKey .
				'&steamids=' . $id;
			$jsonDecoded = json_decode(file_get_contents($url));
			$player = $jsonDecoded->response->players[0];

			$_SESSION['steam']['id'] = $player->steamid;
			$_SESSION['steam']['nickname'] = $player->personaname;
			$_SESSION['steam']['profile'] = $player->profileurl;
			$_SESSION['steam']['avatar'] = $player->avatar;

			$objPlayer = new Player();
			$objPlayer->createPlayerIfNotExists($_SESSION['steam']);

			header('Location: /');
		}
	}
} catch (ErrorException $e) {
	echo $e->getMessage();
}

$heroList = [];
$relationList = [];
if (isset($_SESSION['steam'])) {
	$objHero = new Hero();
	$heroList = $objHero->getHeroesList();
	$objRelation = new Relation();
	$relationList = $objRelation->getRelations();
}

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment(
	$loader,
	[
		'debug' => true,
	]
);
$twig->addExtension(new Twig_Extension_Debug());
$template = $twig->load('index.twig');
echo $template->render(
	[
		'player' => $_SESSION,
		'heroList' => $heroList,
		'relationList' => $relationList,
	]
);
